package com.training.operators;

public class ExampleRelationalOperators {

    public boolean equals(int a, int b) {
        return a == b;
    }

    public boolean notEquals(int a, int b) {
        return a != b;
    }

    public boolean greaterThan(int a, int b) {
        return a > b;
    }

    public boolean lessThan(int a, int b) {
        return a < b;
    }

    public boolean greatThanOrEquals(int a, int b) {
        return a >= b;
    }

    public boolean lessThankOrEquals(int a, int b) {
        return a <= b;
    }
}
