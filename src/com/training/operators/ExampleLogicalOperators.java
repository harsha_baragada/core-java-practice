package com.training.operators;

public class ExampleLogicalOperators {

    public boolean and(boolean a, boolean b) {
        return a && b;
    }

    public boolean or(boolean a, boolean b) {
        return a || b;
    }

    public boolean negotiation(boolean a) {
        return !a;
    }
}
