package com.training.collections.queue;

import java.util.PriorityQueue;
import java.util.Queue;

public class ExampleQueue {

    public void printQueue(){
        Queue<String> strings = new PriorityQueue<>();

        strings.add("Bob");
        strings.add("Peter");
        strings.add("Mark");
        strings.add("Eva");
        strings.add("John");
        strings.add("Laura");
        System.out.println(strings);
        System.out.println(strings.poll());
        System.out.println(strings);
    }
}
