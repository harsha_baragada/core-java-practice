package com.training.collections;

import java.util.ArrayList;
import java.util.Collection;

public class ExampleCollections {

    public void printCollections() {
        Collection<String> strings = new ArrayList<>();
        strings.add("abcd");
        strings.add("jhfj");
        strings.add("utui");
        strings.add("mhjy");
        System.out.println(strings.add("wert"));
        System.out.println(strings.size());
        System.out.println(strings.isEmpty());
        System.out.println(strings.contains("wert"));
        Collection<String> collections = new ArrayList<>();
        collections.add("abcd");
        collections.add("jhfj");
        collections.add("utui");
        System.out.println(strings.containsAll(collections));
        System.out.println(collections.addAll(strings));
        System.out.println(collections);

        System.out.println(collections.hashCode());

    }
}
