package com.training.collections.map;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

public class ExampleMap {

    Map<Integer, String> students = new HashMap<>();
    LinkedHashMap<Integer, String> people = new LinkedHashMap<>();

    public void printMap() {
        students.put(1001, "Bob");
        students.put(1005, "Arva");
        students.put(1004, "Marrie");
        students.put(1006, "Emile");
        students.put(1003, "Mark");
        students.put(1002, "Eva");

        System.out.println(students.size());
        System.out.println(students.get(1005));
        System.out.println(students.keySet());


        people.put(1001, "Bob");
        people.put(1005, "Arva");
        people.put(1004, "Marrie");
        people.put(1006, "Emile");
        people.put(1003, "Mark");
        people.put(1002, "Eva");

        System.out.println(people.size());
        System.out.println(people.get(1005));
        System.out.println(people.keySet());

    }
}
