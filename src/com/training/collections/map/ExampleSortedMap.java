package com.training.collections.map;

import java.util.SortedMap;
import java.util.TreeMap;

public class ExampleSortedMap {

    public void printMap(){
        SortedMap<Integer, String> stringSortedMap = new TreeMap<>();
        stringSortedMap.put(1001, "Bob");
        stringSortedMap.put(1005, "Arva");
        stringSortedMap.put(1004, "Marrie");
        stringSortedMap.put(1006, "Emile");
        stringSortedMap.put(1003, "Mark");
        stringSortedMap.put(1002, "Eva");
        System.out.println(stringSortedMap);
    }
}
