package com.training.collections.set;

import java.util.LinkedHashSet;

public class ExampleLinkedHashSet {
    public void printLinkedHashSet() {
        LinkedHashSet<Integer> integers = new LinkedHashSet<>();
        integers.add(1001);
        integers.add(1004);
        integers.add(1008);
        integers.add(1002);
        integers.add(1001);
        integers.add(1003);
        integers.add(1001);
        System.out.println(integers);
    }
}
