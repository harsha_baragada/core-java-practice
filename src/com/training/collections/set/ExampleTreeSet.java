package com.training.collections.set;

import java.util.ArrayList;
import java.util.Collection;
import java.util.SortedSet;
import java.util.TreeSet;

public class ExampleTreeSet {
    public void printTreeSet() {
        Collection<Integer> duplicates = new ArrayList<>();
        duplicates.add(657567);
        duplicates.add(685675858);
        duplicates.add(67867);
        duplicates.add(78678);
        duplicates.add(785645);
        duplicates.add(67867);
        duplicates.add(685675858);
        duplicates.add(78678);
        duplicates.add(785645);
        duplicates.add(685675858);
        duplicates.add(345345);
        System.out.println(duplicates);
        System.out.println(duplicates.size());
        SortedSet<Integer> integers = new TreeSet<>(duplicates);
        System.out.println(integers);

    }
}
