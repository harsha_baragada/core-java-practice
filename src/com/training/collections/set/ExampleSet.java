package com.training.collections.set;

import java.util.Collection;
import java.util.HashSet;

public class ExampleSet {

    public void printSet() {
        Collection<String> cities = new HashSet<>();
        cities.add("Mumbai");
        cities.add("Mumbai");
        cities.add("Mumbai");
        cities.add("Mumbai");
        System.out.println(cities.size() + " set");
        System.out.println(cities);


    }
}
