package com.training.collections.set;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;

public class ExampleHashSet {

    Collection<Integer> duplicates = new ArrayList<>();

    public void printHashSet() {
        duplicates.add(1001);
        duplicates.add(1002);
        duplicates.add(1001);
        duplicates.add(1001);
        duplicates.add(1002);
        duplicates.add(1001);
        System.out.println(duplicates.size());
        System.out.println(duplicates);
        HashSet<Integer> ids = new HashSet<>(duplicates);
        System.out.println(ids);
        System.out.println(ids.size());

    }
}
