package com.training.collections.list;

import java.util.ArrayList;
import java.util.List;

public class ExampleList {

    List<Integer> list = new ArrayList();

    public void printList() {
        list.add(1002);
        list.add(1056);
        list.add(1001);
        list.add(153);
        list.add(4535);
        list.add(6634);
        list.add(1001);
        System.out.println(list);
        System.out.println(list.indexOf(1001));
        System.out.println(list.lastIndexOf(1001));
    }
}
