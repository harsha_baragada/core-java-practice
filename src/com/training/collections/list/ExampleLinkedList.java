package com.training.collections.list;

import java.util.LinkedList;

public class ExampleLinkedList {
    public void printLinkedList(){
        LinkedList<String> linkedList = new LinkedList<>();

        linkedList.add("Mumbai");
        linkedList.add("Delhi");
        linkedList.add("Bengluru");
        linkedList.add("Kolkata");
        linkedList.add("Hyderabad");
        linkedList.add("Chennai");
        linkedList.add("Surat");
        System.out.println(linkedList);
    }
}
