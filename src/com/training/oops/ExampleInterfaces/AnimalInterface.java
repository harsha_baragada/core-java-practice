package com.training.oops.ExampleInterfaces;

public interface AnimalInterface {

    String helloFromAnimal();

    String animalShouts();
}
