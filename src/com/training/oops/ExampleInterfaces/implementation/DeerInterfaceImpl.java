package com.training.oops.ExampleInterfaces.implementation;

import com.training.oops.ExampleInterfaces.AnimalInterface;
import com.training.oops.ExampleInterfaces.DeerInterface;

public class DeerInterfaceImpl implements DeerInterface, AnimalInterface {

    @Override
    public String helloFromAnimal() {
        return "This is from animal";
    }

    @Override
    public String animalShouts() {
        return null;
    }

    @Override
    public String helloFromMammal() {
        return "This is from mammal ";
    }
}
