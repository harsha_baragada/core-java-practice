package com.training.oops.polymorphism;

public class Salary extends Employee {

    private double salary;


    public Salary(int id, String name, String address) {
        super(id, name, address);
        setSalary(salary);
    }

    public double getSalary() {
        return salary;
    }

    public void setSalary(double newSalary) {
        if (newSalary >= 0.0) {
            this.salary = salary;
        }

    }

    public void mailCheck() {
        System.out.println("Mail checking in the child class");
        System.out.println("Mail checking " + getName() + " " + getAddress());
    }
}
