package com.training.oops.abstraction;

public class AbstractImplementation extends ExampleAbstractClass {


    @Override
    public String getData() {
        return "returning the data";
    }
}
