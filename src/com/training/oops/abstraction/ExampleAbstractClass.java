package com.training.oops.abstraction;

public abstract class ExampleAbstractClass {

    public abstract String getData();

    public String getRandomString() {
        return "Hey";
    }

}
