package com.training.oops.inheritance;

public class Mammal extends Animal {

    public Mammal(String name, int id) {
        super(name, id);
    }

    public Mammal() {
        super();
    }

    public String sayMammal() {
        return "This is from mammal class";
    }


    public void move() {
        System.out.println("Mammals can move");
    }
}
