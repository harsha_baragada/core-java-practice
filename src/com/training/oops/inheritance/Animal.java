package com.training.oops.inheritance;

public class Animal {
    public String name;
    public int id;

    public Animal(String name, int id) {
        this.name = name;
        this.id = id;
    }

    public Animal() {
    }

    public String exampleAnimal() {
        return "This method is from animal class";
    }

    public void printAnimalNameAndId() {
        System.out.println("Animal Name : " + name + " with id" + id);
    }
}
