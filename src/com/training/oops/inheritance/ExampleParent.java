package com.training.oops.inheritance;

public class ExampleParent {

    public String sayHelloFromParent() {
        return "Parent says hello";
    }
}
