package com.training.oops.inheritance;

public class Deer extends Mammal {
    public Deer(String name, int id) {
        super(name, id);
    }

    public Deer() {
        super();
    }

    public void move() {
        super.move();
        System.out.println("Deer can move");
    }

    public void eatsGrass() {
        System.out.println("The deer eats grass");
    }
}
