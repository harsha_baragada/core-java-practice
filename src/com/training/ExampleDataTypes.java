package com.training;

public class ExampleDataTypes {

    public static int I = 20;
    protected final boolean FLAG = true;
    short aShort = 3448;
    int anInt = 323792392;
    long aLong = 374994848848940009l;
    float aFloat = 348.4f;
    double aDouble = 38383.74;
    char aChar = 'e';
    boolean aBoolean = true;
    private byte aByte = 127;


    public ExampleDataTypes() {
        System.out.println("This is from the default constructor of the class");
    }

    public ExampleDataTypes(byte aByte, short aShort, int anInt, long aLong, float aFloat, double aDouble, char aChar, boolean aBoolean) {
        this.aByte = aByte;
        this.aShort = aShort;
        this.anInt = anInt;
        this.aLong = aLong;
        this.aFloat = aFloat;
        this.aDouble = aDouble;
        this.aChar = aChar;
        this.aBoolean = aBoolean;
    }

    public void printVariable() {
        System.out.println(aByte);
        System.out.println(aShort);
        System.out.println(anInt);
        System.out.println(aLong);
        System.out.println(aFloat);
        System.out.println(aDouble);
        System.out.println(aChar);
        System.out.println(aBoolean);
    }

}
