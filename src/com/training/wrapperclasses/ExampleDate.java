package com.training.wrapperclasses;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public class ExampleDate {


    public void printDates() throws InterruptedException {
        Date today = new Date();
        System.out.println(today);
        Thread.sleep(10000);
        Date later = new Date();
        System.out.println(later);
        System.out.println(today.after(later));
        System.out.println(today.before(later));
        System.out.println(today.getTime());
        System.out.println(today.equals(later));

        SimpleDateFormat dateFormat = new SimpleDateFormat("E yyyy.MM.dd 'at' hh:mm:ss  & zzz");

        System.out.println(dateFormat.format(today));
        GregorianCalendar calendar = new GregorianCalendar();
    }
}
