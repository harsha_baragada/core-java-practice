package com.training.wrapperclasses;

public class ExampleArrays {

    String[] cities = {"Newyork", "London", "Mumbai", "Delhi", "Amsterdam"}; //preferred way
    Integer ids[] = {1001, 1002, 1003, 1004, 1005, 1006, 1007, 1008}; // works but not a preferable way

    public void printArrays() {
        System.out.println(cities.length);
        cities[2] = null;
        System.out.println(cities.length);
        for (String city : cities
        ) {
            System.out.println(city);

        }
    }
}
