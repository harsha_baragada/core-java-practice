package com.training.wrapperclasses;

public class ExampleWrapperClasses {

    Number number = 1001;
    Double aDouble = new Double(1001.45);
    Byte aByte = 023;
    Integer integer = 100220;
    Short aShort = 338;
    Float aFloat = 399.3F;
    float aFloat1 = 344.4F;
    Character character = 'R';
    char aChar = 'W';

    public void printWrapperClasses() {
        System.out.println(number);
    }
}
