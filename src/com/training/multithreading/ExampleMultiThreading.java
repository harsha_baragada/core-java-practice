package com.training.multithreading;

public class ExampleMultiThreading implements Runnable {

    private Thread t;
    private String threadName;

    public ExampleMultiThreading(String threadName) {
        this.threadName = threadName;
        System.out.println("creating a thread with threadName " + threadName);
    }

    @Override
    public void run() {

        System.out.println("running " + threadName);

        try {
            for (int i = 4; i > 0; i--) {
                System.out.println("Thread " + threadName + " -" + i);
                Thread.sleep(50);
            }
        } catch (InterruptedException e) {
            System.out.println("Thread " + threadName + " is interrupted");
        }
        System.out.println("Thread " + threadName + "Exiting");

    }

    public void start() {
        System.out.println("Starting " + threadName);
        if (t == null) {
            t = new Thread(this, threadName);
            t.start();
        }
    }
}
