package com.training.generics;

public class ExampleGenericMethod {
    public static <E> void printArray(E[] array){

        for (E e: array
             ) {
            System.out.println(e);
        }

    }
}
