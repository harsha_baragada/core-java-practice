package com.training;


import com.training.multithreading.ExampleMultiThreading;

public class Main {

    public static void main(String[] args) {

        ExampleMultiThreading r1 = new ExampleMultiThreading("Thread- 1");
        r1.start();

        ExampleMultiThreading r2 = new ExampleMultiThreading("Thread-2 ");
        r2.start();

    }
}

