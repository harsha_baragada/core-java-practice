package com.training.loops;

public class ExampleWhileLoop {

    int x = 20;

    public void printWhileLoop() {

        while (x < 30) {
            System.out.println(x);
            x++;
        }

        do {
            System.out.println(x);
            x++;
        }
        while (x < 30);
    }
}
