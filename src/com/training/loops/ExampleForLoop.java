package com.training.loops;

public class ExampleForLoop {

    public void printForLoop() {

        String[] fruits = {"Apple", "Banana", "Orange", "Papaya", "Pears"};

        for (int i = 0; i < fruits.length; i++) {
            if (fruits[i].equals("Papaya")) {
                continue;
            }
            System.out.println(fruits[i]);
        }


    }
}
