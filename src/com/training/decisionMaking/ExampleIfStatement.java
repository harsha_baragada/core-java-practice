package com.training.decisionMaking;

public class ExampleIfStatement {

    int i = 30;

    boolean ageFlag;

    public void printStatements() {

        if (i == 10) {
            System.out.println(i);
        } else if (i == 15) {
            System.out.println("I is 15");
        } else if (i == 20) {
            System.out.println("The value of I is " + i);
        } else {
            System.out.println("the above if conditions is false that's why I am getting printed");

        }
    }

    public void printNestedStatement() {

        if (i % 10 == 0) {
            System.out.println("I is divided by 10");
            if (i % 5 == 0) {
                System.out.println("I is divided by 5");
                if (i % 3 == 0) {
                    System.out.println("I is divided by 3");
                }
            }

        } else {

            System.out.println("I is not divided by 10");
            if (i % 4 == 0) {
                System.out.println("I is divided by 4 ");
            } else {
                System.out.println("I is not divided by 4");
            }
        }

    }

    public void exampleSwitchCaseStatement() {

        char grade = 'T';

        switch (grade) {
            case 'A':
                System.out.println("Excellent");
                break;
            case 'B':
                System.out.println("Very Good");
                break;
            case 'C':
                System.out.println("Good");
                break;
            case 'D':
                System.out.println("Satisfactory");
                break;
            case 'E':
                System.out.println("Failed");
                break;
            default:
                System.out.println("This grade is not in the system please contract your admin for more info");
        }

        ageFlag = (i > 30) ? true : false;
        System.out.println(ageFlag);

    }
}
