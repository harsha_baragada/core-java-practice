package com.training;

public abstract class ExampleAbstractClass {

    public String getName() {
        return "John";
    }

    public abstract int getNumber();
}
