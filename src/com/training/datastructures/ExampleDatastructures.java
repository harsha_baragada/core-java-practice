package com.training.datastructures;

import java.util.*;

public class ExampleDatastructures {

    public void printDays() {
        Enumeration days;
        Vector dayNames = new Vector();
        dayNames.add("Sunday");
        dayNames.add("Monday");
        dayNames.add("Tuesday");
        dayNames.add("Wednesday");
        dayNames.add("Thursday");
        dayNames.add("Friday");
        dayNames.add("Saturday");
        days = dayNames.elements();
        while (days.hasMoreElements()) {
            System.out.println(days.nextElement());
        }

        Stack<String> cities = new Stack<>();
        cities.push("Mumbai");
        cities.push("Delhi");
        cities.push("Hyderabad");
        cities.push("Bengaluru");
        System.out.println(cities);
        System.out.println(cities.pop());
        System.out.println(cities);
        System.out.println(cities.search("Bengaluru"));


    }

    public void printDictionary() {
        Dictionary<Integer, String> dictionary = new Hashtable<>();
        System.out.println("Empty dictionary " + dictionary.isEmpty());
        dictionary.put(1001, "Apple");
        dictionary.put(1002, "Banana");
        dictionary.put(1003, "Pears");
        dictionary.put(1004, "Strawberry");
        dictionary.put(1005, "Mango");

        System.out.println(dictionary.size());
        Enumeration<Integer> keys = dictionary.keys();
        while (keys.hasMoreElements()) {
            System.out.println(keys.nextElement());
        }
        Enumeration<String> values = dictionary.elements();
        while (values.hasMoreElements()) {
            System.out.println(values.nextElement());
        }

        System.out.println(System.getProperties());

    }
}
