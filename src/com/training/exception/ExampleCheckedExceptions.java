package com.training.exception;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class ExampleCheckedExceptions {


    public void unCheckedException() {
        String[] cities = {"Newyork", "London", "Mumbai", "Tel Aviv", "Amsterdam"};
        System.out.println(cities[4]);
    }

    public void readFile() throws FileNotFoundException {
        File file = new File("E://spring_file.txt");
        FileReader fileReader = new FileReader(file);
    }

}
